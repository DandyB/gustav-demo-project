package ee.simple.project.helper;

import ee.simple.project.dto.UserDto;
import ee.simple.project.model.User;
import ee.simple.project.service.mapper.UserMapper;

import java.time.LocalDate;

public class UserTestUtils {

    public static User getNewUser() {
        LocalDate dateOfBirth = LocalDate.of(1990, 10, 7);

        User user = new User();
        user.setUsername("President of the Galaxy");
        user.setFirstName("Zaphod");
        user.setLastName("Beeblebrox");
        user.setDateOfBirth(dateOfBirth);
        return user;
    }

    public static UserDto getNewUserDto() {
        return UserMapper.INSTANCE.toDto(getNewUser());
    }

}
