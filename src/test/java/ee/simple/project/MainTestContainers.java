package ee.simple.project;

import lombok.extern.slf4j.Slf4j;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;

@ActiveProfiles("test")
@Slf4j
public abstract class MainTestContainers {

    private static final int EXPOSED_RABBITMQ_PORT = 5672;

    static final PostgreSQLContainer<?> postgres;

    static final GenericContainer<?> rabbitMq;

    static {
        postgres = new PostgreSQLContainer<>("postgres:latest");
        rabbitMq = new GenericContainer<>("rabbitmq:management")
                .withExposedPorts(EXPOSED_RABBITMQ_PORT);

        rabbitMq.start();
        postgres.start();

        System.setProperty("spring.datasource.url", postgres.getJdbcUrl());
        System.setProperty("spring.datasource.username", postgres.getUsername());
        System.setProperty("spring.datasource.password", postgres.getPassword());

        System.setProperty("spring.rabbitmq.host", rabbitMq.getContainerIpAddress());
        System.setProperty("spring.rabbitmq.port", rabbitMq.getMappedPort(EXPOSED_RABBITMQ_PORT).toString());
    }

}
