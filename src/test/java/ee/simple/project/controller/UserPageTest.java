package ee.simple.project.controller;

import ee.simple.project.MainTestContainers;
import ee.simple.project.dto.UserDto;
import ee.simple.project.model.User;
import ee.simple.project.repository.UserRepository;
import ee.simple.project.service.RabbitSender;
import ee.simple.project.service.mapper.UserMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.org.apache.commons.lang.RandomStringUtils;

import java.time.LocalDate;

import static ee.simple.project.helper.UserTestUtils.getNewUser;
import static ee.simple.project.helper.UserTestUtils.getNewUserDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserPageTest extends MainTestContainers {

    private static final String INDEX_URL = "/";
    private static final String INDEX_VIEW = "index";
    private static final String REDIRECT_INDEX_VIEW = "redirect:/";
    private static final String HTML_DOCTYPE = "<!DOCTYPE html>";
    private static final String NO_USER_MSG = "There are no users";
    private static final String NEW_USER_MODEL = "newUser";
    private static final String USERS_MODEL = "users";
    private static final String USERNAME_FIELD = "username";
    private static final String FIRST_NAME_FIELD = "firstName";
    private static final String LAST_NAME_FIELD = "lastName";
    private static final String DATE_OF_BIRTH_FIELD = "dateOfBirth";
    private static final String BACK_HOME_MSG = "Back to Home";
    private static final String OOPS_MSG = "Oops!";
    private static final String USERNAME_ALREADY_EXISTS_MSG = "Username already exists: ";
    private static final int BIGGER_THAN_MAX_FIELD_LENGTH = 51;

    @Autowired
    private UserRepository userRep;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RabbitSender rabbitSender;

    @AfterEach
    public void after() {
        userRep.deleteAll();
    }

    private void checkIfMainPageUsersAreEmpty() throws Exception {
        String html = mockMvc.perform(get(INDEX_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat(html).contains(HTML_DOCTYPE);
        assertThat(html).doesNotContain("404");
        assertThat(html).contains(NO_USER_MSG);
    }

    @Test
    @Transactional
    public void testRabbitSendReceive() {
        UserDto newUserDto = getNewUserDto();
        rabbitSender.send(newUserDto);

        User createdUser = userRep.findByUsername(newUserDto.getUsername());
        assertThat(createdUser).isNotNull();
    }

    @Test
    public void testIndexPage_noUsers() throws Exception {
        String html = mockMvc.perform(get(INDEX_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(INDEX_VIEW))
                .andReturn().getResponse().getContentAsString();

        assertThat(html).contains(HTML_DOCTYPE);
        assertThat(html).doesNotContain("404");
        assertThat(html).contains(NO_USER_MSG);
    }

    @Test
    public void testIndexPage_preexistingUser() throws Exception {
        UserDto existingUserDto = UserMapper.INSTANCE.toDto(userRep.save(getNewUser()));

        String html = mockMvc.perform(get(INDEX_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(INDEX_VIEW))
                .andExpect(model().attribute(USERS_MODEL, contains(existingUserDto)))
                .andReturn().getResponse().getContentAsString();

        assertThat(html).contains(HTML_DOCTYPE);
        assertThat(html).doesNotContain("404");
        assertThat(html).doesNotContain(NO_USER_MSG);
    }

    @Test
    public void testIndexPage_addUser() throws Exception {
        UserDto newUserDto = getNewUserDto();
        checkIfMainPageUsersAreEmpty();

        mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, newUserDto.getUsername())
                .param(FIRST_NAME_FIELD, newUserDto.getFirstName())
                .param(LAST_NAME_FIELD, newUserDto.getLastName())
                .param(DATE_OF_BIRTH_FIELD, newUserDto.getDateOfBirth().toString()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name(REDIRECT_INDEX_VIEW));

        User createdUser = userRep.findByUsername(newUserDto.getUsername());
        assertThat(createdUser).isNotNull();

        mockMvc.perform(get(INDEX_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(INDEX_VIEW))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute(NEW_USER_MODEL, new UserDto()))
                .andExpect(model().attribute(USERS_MODEL, contains(UserMapper.INSTANCE.toDto(createdUser))));
    }

    @Test
    public void testIndexPage_addUserCheckTrim() throws Exception {
        UserDto newUserDto = getNewUserDto();
        checkIfMainPageUsersAreEmpty();

        String tenSpaces="          ";

        mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, tenSpaces + newUserDto.getUsername() + tenSpaces)
                .param(FIRST_NAME_FIELD, tenSpaces + newUserDto.getFirstName() + tenSpaces)
                .param(LAST_NAME_FIELD, tenSpaces + newUserDto.getLastName() + tenSpaces)
                .param(DATE_OF_BIRTH_FIELD, newUserDto.getDateOfBirth().toString()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name(REDIRECT_INDEX_VIEW));

        User createdUser = userRep.findByUsername(newUserDto.getUsername());
        assertThat(createdUser).isNotNull();

        mockMvc.perform(get(INDEX_URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(INDEX_VIEW))
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute(NEW_USER_MODEL, new UserDto()))
                .andExpect(model().attribute(USERS_MODEL, contains(UserMapper.INSTANCE.toDto(createdUser))));
    }

    @Test
    public void testIndexPage_usernameAlreadyExistsError() throws Exception {
        UserDto existingUserDto = UserMapper.INSTANCE.toDto(userRep.save(getNewUser()));

        String html = mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, existingUserDto.getUsername())
                .param(FIRST_NAME_FIELD, existingUserDto.getFirstName())
                .param(LAST_NAME_FIELD, existingUserDto.getLastName())
                .param(DATE_OF_BIRTH_FIELD, existingUserDto.getDateOfBirth().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat(html).contains(HTML_DOCTYPE);
        assertThat(html).doesNotContain("500");
        assertThat(html).contains(OOPS_MSG);
        assertThat(html).contains(USERNAME_ALREADY_EXISTS_MSG + existingUserDto.getUsername());
        assertThat(html).contains(BACK_HOME_MSG);
    }

    @Test
    public void testIndexPage_404error() throws Exception {
        UserDto existingUserDto = UserMapper.INSTANCE.toDto(userRep.save(getNewUser()));

        mockMvc.perform(post("/does-not-exist")
                .param(USERNAME_FIELD, existingUserDto.getUsername())
                .param(FIRST_NAME_FIELD, existingUserDto.getFirstName())
                .param(LAST_NAME_FIELD, existingUserDto.getLastName())
                .param(DATE_OF_BIRTH_FIELD, existingUserDto.getDateOfBirth().toString()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testIndexPage_addUser_emptyFormFields() throws Exception {
        String emptyString = "     ";

        mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, emptyString)
                .param(FIRST_NAME_FIELD, emptyString)
                .param(LAST_NAME_FIELD, emptyString)
                .param(DATE_OF_BIRTH_FIELD, emptyString))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, USERNAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, FIRST_NAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, LAST_NAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, DATE_OF_BIRTH_FIELD, "NotNull"));
    }

    @Test
    public void testIndexPage_addUser_nullFormFields() throws Exception {
        String nullData = null;

        mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, nullData)
                .param(FIRST_NAME_FIELD, nullData)
                .param(LAST_NAME_FIELD, nullData)
                .param(DATE_OF_BIRTH_FIELD, nullData))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, USERNAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, FIRST_NAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, LAST_NAME_FIELD, "NotBlank"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, DATE_OF_BIRTH_FIELD, "NotNull"));
    }

    @Test
    public void testIndexPage_addUser_invalidFormFields() throws Exception {
        String longString = RandomStringUtils.randomAlphanumeric(BIGGER_THAN_MAX_FIELD_LENGTH);
        LocalDate futureDate = LocalDate.of(2035, 10, 7);

        mockMvc.perform(post(INDEX_URL)
                .param(USERNAME_FIELD, longString)
                .param(FIRST_NAME_FIELD, longString)
                .param(LAST_NAME_FIELD, longString)
                .param(DATE_OF_BIRTH_FIELD, futureDate.toString()))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, USERNAME_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, FIRST_NAME_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, LAST_NAME_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(NEW_USER_MODEL, DATE_OF_BIRTH_FIELD, "Past"));
    }

}
