package ee.simple.project.rabbit;

import ee.simple.project.MainTestContainers;
import ee.simple.project.dto.UserDto;
import ee.simple.project.error.GeneralApiException;
import ee.simple.project.service.RabbitReceiver;
import ee.simple.project.service.RabbitSender;
import ee.simple.project.service.UserService;
import io.github.netmikey.logunit.api.LogCapturer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static ee.simple.project.helper.UserTestUtils.getNewUserDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@ActiveProfiles("test")
public class RabbitTest extends MainTestContainers {

    private static final String TEST_GENERAL_API_EXCEPTION_MSG = "test error message";
    private static final String DLQ_CONSUME_MSG = "Received failed message: ";

    @Autowired
    private RabbitSender rabbitSender;

    @MockBean
    private UserService userService;

    @SpyBean
    private RabbitReceiver rabbitReceiver;

    @RegisterExtension
    LogCapturer logs = LogCapturer.create().captureForType(RabbitReceiver.class);

    @Test
    public void testRabbit_receiveGeneralApiException() {
        UserDto newUserDto = getNewUserDto();
        doThrow(new GeneralApiException(TEST_GENERAL_API_EXCEPTION_MSG)).doNothing().when(userService).addUser(newUserDto);

        sendAndExpectGeneralApiException(newUserDto, TEST_GENERAL_API_EXCEPTION_MSG);
    }

    @Test
    public void testRabbit_receiveNullPointerException() {
        UserDto newUserDto = getNewUserDto();
        doThrow(new NullPointerException()).doNothing().when(userService).addUser(newUserDto);

        sendAndExpectGeneralApiException(newUserDto, "");
    }

    @Test
    public void testRabbitDLQ_unhandledException() {
        UserDto newUserDto = getNewUserDto();
        doThrow(new NullPointerException()).doReturn(null).when(rabbitReceiver).receiveMessage(newUserDto);

        sendAndExpectRuntimeException(newUserDto);
        logs.assertContains(DLQ_CONSUME_MSG + newUserDto);
    }

    private void sendAndExpectRuntimeException(UserDto newUserDto) {
        try {
            rabbitSender.send(newUserDto);
        } catch (Exception e) {
            assertThat(e).isExactlyInstanceOf(RuntimeException.class);
        }
    }

    private void sendAndExpectGeneralApiException(UserDto newUserDto, String errMsg) {
        try {
            rabbitSender.send(newUserDto);
        } catch (Exception e) {
            assertThat(e).isExactlyInstanceOf(GeneralApiException.class);
            assertThat(e.getMessage()).isEqualTo(errMsg);
        }
    }

}
