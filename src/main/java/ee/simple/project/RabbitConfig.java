package ee.simple.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Value("${spring.rabbitmq.queue}")
    private String queue;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.rabbitmq.routingKey}")
    private String routingKey;

    @Value("${spring.rabbitmq.dlQueue}")
    private String dlQueue;

    @Value("${spring.rabbitmq.dlExchange}")
    private String dlExchange;

    @Value("${spring.rabbitmq.dlRoutingKey}")
    private String dlRoutingKey;

    @Value("${spring.rabbitmq.message-ttl}")
    private int messageTtl;

    @Bean
    public Queue queue() {
        return QueueBuilder.durable(queue)
                .ttl(messageTtl)
                .deadLetterExchange(dlExchange)
                .deadLetterRoutingKey(dlRoutingKey).build();
    }

    @Bean
    public Queue dlQueue() {
        return QueueBuilder.durable(dlQueue).build();
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    public DirectExchange dlExchange() {
        return new DirectExchange(dlExchange);
    }

    @Bean
    public Binding binding(Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }

    @Bean
    public Binding dlBinding() {
        return BindingBuilder.bind(dlQueue()).to(dlExchange()).with(dlRoutingKey);
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2MessageConverter() {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
        return new Jackson2JsonMessageConverter(mapper);
    }

}
