package ee.simple.project.controller;

import ee.simple.project.dto.UserDto;
import ee.simple.project.service.RabbitSender;
import ee.simple.project.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping({  "/" })
@Slf4j
public class IndexController {

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitSender rabbitSender;

    @GetMapping
    public String main(Model model) {
        log.info("Init new main page");

        model.addAttribute("newUser", new UserDto());
        model.addAttribute("users", userService.findAll());
        return "index";
    }

    @PostMapping
    public String addUser(@Valid @ModelAttribute("newUser") UserDto newUser, BindingResult bindingResult) {
        log.info("Add new user: {}", newUser);

        if (bindingResult.hasErrors()) {
            return "index";
        }

        rabbitSender.send(newUser);
        return "redirect:/";
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

}
