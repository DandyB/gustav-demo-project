package ee.simple.project.controller;

import ee.simple.project.dto.ErrorInfo;
import ee.simple.project.error.GeneralApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
@Slf4j
public class ExceptionsController {
    private static final String GENERAL_APP_EXCEPTION_MSG = "Application has encountered an error";
    private static final String OOPS_CODE = "Oops!";

    @ExceptionHandler(Exception.class)
    public String processGeneralException(Exception ex, Model model) {
        log.error("Exception was thrown: {}", getExceptionInfo(ex));
        return initErrorModelAndGetTemplate(GENERAL_APP_EXCEPTION_MSG, model);
    }

    @ExceptionHandler(GeneralApiException.class)
    public String processGeneralApiException(GeneralApiException ex, Model model) {
        log.error("A general API exception was thrown: {}", getExceptionInfo(ex));

        String msg = ObjectUtils.isEmpty(ex.getMessage()) ? GENERAL_APP_EXCEPTION_MSG : ex.getMessage();
        return initErrorModelAndGetTemplate(msg, model);
    }

    private String initErrorModelAndGetTemplate(String message, Model model) {
        ErrorInfo info = ErrorInfo.builder()
                .code(OOPS_CODE)
                .message(message)
                .build();

        model.addAttribute("errorInfo", info);
        return "error";
    }

    private String getExceptionInfo(Exception ex) {
        return ex.getMessage() == null ? Arrays.toString(ex.getStackTrace()) : ex.getMessage();
    }

}
