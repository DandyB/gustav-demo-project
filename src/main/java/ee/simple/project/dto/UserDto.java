package ee.simple.project.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
public class UserDto {

    private static final String CANT_BE_EMPTY_MSG = "Field can not be empty";
    private static final String MUST_BE_BETWEEN_2_AND_50_CHAR_MSG = "Must be between 2 and 50 characters";
    private static final String MUST_BE_IN_THE_PAST_MSG = "Must be in the past";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @NotBlank(message = CANT_BE_EMPTY_MSG)
    @Size(min = 2, max = 50, message = MUST_BE_BETWEEN_2_AND_50_CHAR_MSG)
    private String username;

    @NotBlank(message = CANT_BE_EMPTY_MSG)
    @Size(min = 2, max = 50, message = MUST_BE_BETWEEN_2_AND_50_CHAR_MSG)
    private String firstName;

    @NotBlank(message = CANT_BE_EMPTY_MSG)
    @Size(min = 2, max = 50, message = MUST_BE_BETWEEN_2_AND_50_CHAR_MSG)
    private String lastName;

    @NotNull(message = CANT_BE_EMPTY_MSG)
    @DateTimeFormat(pattern = DATE_FORMAT)
    @Past(message = MUST_BE_IN_THE_PAST_MSG)
    private LocalDate dateOfBirth;

    private ZonedDateTime created;

}
