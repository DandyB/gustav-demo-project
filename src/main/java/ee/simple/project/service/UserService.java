package ee.simple.project.service;

import ee.simple.project.dto.UserDto;
import ee.simple.project.error.GeneralApiException;
import ee.simple.project.model.User;
import ee.simple.project.repository.UserRepository;
import ee.simple.project.service.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRep;

    public List<UserDto> findAll() {
        List<User> users = userRep.findAll();
        return UserMapper.INSTANCE.toDto(users);
    }

    public void existByUsernameOrThrow(String username) {
        if (userRep.existsByUsername(username)) {
            throw new GeneralApiException("Username already exists: " + username);
        }
    }

    public void addUser(UserDto userDto) {
        log.info("addUser is happening: " + userDto);
        existByUsernameOrThrow(userDto.getUsername());
        User userEntity = UserMapper.INSTANCE.toEntity(userDto);
        userRep.save(userEntity);
    }

}
