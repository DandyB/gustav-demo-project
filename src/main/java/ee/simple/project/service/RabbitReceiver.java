package ee.simple.project.service;

import ee.simple.project.dto.UserDto;
import ee.simple.project.dto.UserResponse;
import ee.simple.project.error.GeneralApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitReceiver {

    @Autowired
    private UserService userService;

    @RabbitListener(queues = "${spring.rabbitmq.dlQueue}")
    public void processFailedMessages(UserDto userDto) {
        log.info("Received failed message: {}", userDto.toString());
        // remove messages from the DLQ; main purpose: discard of poison and expired messages
        // Spring Cloud Stream could be implemented with parameter republishToDlq = true
        // this will enable failed messages to be republished to the DLQ with error info
        // instead of just being rejected
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public UserResponse receiveMessage(UserDto userDto) {
        try {
            log.info("Trying to add new user: " + userDto);
            userService.addUser(userDto);
            return new UserResponse();
        } catch (GeneralApiException e) {
            logResponseError(userDto);
            return new UserResponse(e.getMessage());
        } catch (Exception e) {
            logResponseError(userDto);
            return new UserResponse("");
        }
    }

    private void logResponseError(UserDto userDto) {
        log.error("Something went wrong while trying to add user: {}", userDto);
    }

}
