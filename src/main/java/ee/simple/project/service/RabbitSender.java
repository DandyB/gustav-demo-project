package ee.simple.project.service;

import ee.simple.project.dto.UserDto;
import ee.simple.project.dto.UserResponse;
import ee.simple.project.error.GeneralApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.rabbitmq.routingKey}")
    private String routingKey;

    public void send(UserDto userDto) {
        UserResponse response = rabbitTemplate.convertSendAndReceiveAsType(
                exchange,
                routingKey,
                userDto,
                new ParameterizedTypeReference<>() {
                });

        checkResponseForErrors(response);
    }

    private void checkResponseForErrors(UserResponse response) {
        if (response == null) {
            log.error("User response was null");
            throw new RuntimeException();
        }

        if (response.getErrorMsg() != null) {
            throw new GeneralApiException(response.getErrorMsg());
        }
    }

}
