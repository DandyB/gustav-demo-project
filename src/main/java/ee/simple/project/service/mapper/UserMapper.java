package ee.simple.project.service.mapper;

import ee.simple.project.dto.UserDto;
import ee.simple.project.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(ignore = true, target = "id")
    User toEntity(UserDto userDto);

    UserDto toDto(User userEntity);

    List<UserDto> toDto(List<User> userEntityList);

}
