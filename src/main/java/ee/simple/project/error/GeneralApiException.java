package ee.simple.project.error;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class GeneralApiException extends RuntimeException {

    private static final long serialVersionUID = -2723306214423270818L;

    public GeneralApiException(String message) {
        super(message);
    }

}
