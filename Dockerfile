FROM gradle:jdk11 as gradleimage
COPY . /home/gradle/source
WORKDIR /home/gradle/source
RUN gradle build -x test
# test had to be disabled, because of the use of test containers.
# tests could be configured to run on CI prior to building a docker image.

FROM openjdk:11-jre-slim
COPY --from=gradleimage /home/gradle/source/build/libs/ee-simple-project-1.0-SNAPSHOT.jar  /app/
WORKDIR /app
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "ee-simple-project-1.0-SNAPSHOT.jar"]
