## Setup

This is a simple Java Spring-Boot Gradle project with Docker Compose.\
It utilizes RabbitMQ and Postgres.

* Navigate to the root folder of this project
* Run `docker-compose up`
* Visit http://localhost:8090

## Tests

This project utilizes Testcontainers.\
A Docker daemon needs to be running and discoverable for the tests to be able to run.
